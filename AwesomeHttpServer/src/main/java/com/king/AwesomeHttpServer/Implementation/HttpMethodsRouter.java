package com.king.AwesomeHttpServer.Implementation;

// Imports
import java.io.IOException;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * Implements method parsing and distribution to specific
 * GET, POST, PUT, DELETE etc. method handlers. To be
 * worked-on;
 */
public class HttpMethodsRouter implements HttpHandler {

	@ Override
	public void handle (final HttpExchange oneExchange) throws IOException {
		// From time to time, clean-up
		Session.triggerSessionCleanup ();

		// Defaults
		final String cachedRequestMethod = oneExchange.getRequestMethod ();
		final Boolean isGetRequest = cachedRequestMethod.compareTo (new String ("GET")) == 0;
		final Boolean isPostRequest = cachedRequestMethod.compareTo (new String ("POST")) == 0;

		// Default
		HttpHandler oneHandler = null;
		Boolean didNotRoute = false;

		// Check
		if (isGetRequest) {
			// Route to a GET handler
			oneHandler = new HttpMethodGet ();
		} else if (isPostRequest) {
			// Route to a POST handler
			oneHandler = new HttpMethodPost ();
		} else {
			// Default
			didNotRoute = true;
			DefaultYoMamaJoke.yoMama (oneExchange);
		}

		// Check
		if (!(didNotRoute)) {
			// Handle
			oneHandler.handle (oneExchange);
		}
	}
}
