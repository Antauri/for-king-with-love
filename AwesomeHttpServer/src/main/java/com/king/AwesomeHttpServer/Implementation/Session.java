package com.king.AwesomeHttpServer.Implementation;

// Imports
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import lombok.Getter;

/**
 * Contains the specific implementation of our session
 * in the way of Concurrent containers for the different mappings
 * that we require in the application;
 */
public class Session {

	// Privates
	@ Getter private static final ConcurrentHashMap<Long, UUID> userToSessionMapping = new ConcurrentHashMap<Long, UUID> ();
	@ Getter private static final ConcurrentHashMap<UUID, Long> sessionToUserMapping = new ConcurrentHashMap<UUID, Long> ();
	@ Getter private static final ConcurrentSkipListMap<Long, UUID> dateToExpiringUserSession = new ConcurrentSkipListMap<Long, UUID> ();
	private static final Long millisToEraseSessionsAfter = 600000L;

	// Clean-up any expired sessions
	public static void triggerSessionCleanup () {
		// Check time now, and minus 10 minutes
		final ConcurrentNavigableMap<Long, UUID> mapView = dateToExpiringUserSession
			.headMap (new Date ().getTime () - millisToEraseSessionsAfter, true); // 10 minutes ago

		// Go through each
		for (final Long oneKey : mapView.keySet ()) {
			// Remove
			final UUID oneSessionKey = dateToExpiringUserSession.get (oneKey);
			dateToExpiringUserSession.remove (oneKey);

			// Remove from sessions and users
			final Long oneUserId = sessionToUserMapping.get (oneSessionKey);
			sessionToUserMapping.remove (oneSessionKey);
			userToSessionMapping.remove (oneUserId);
		}
	}
}
