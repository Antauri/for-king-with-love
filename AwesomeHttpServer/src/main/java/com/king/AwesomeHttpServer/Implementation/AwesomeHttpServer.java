package com.king.AwesomeHttpServer.Implementation;

import java.net.InetAddress;
// Imports
import java.net.InetSocketAddress;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import com.sun.net.httpserver.HttpServer;
import lombok.extern.log4j.Log4j;

/**
 * Implementation takes care of setting up our end-points. I've
 * gone a little further here and since you can only have 64k concurrent
 * connections per (source, source-port, dest, dest-port) I've decided
 * to make this server listen on multiple ports at a time. Thus, from one
 * testing machine you can have upwards for 64k connections (given that the
 * machine can take it). See the ephemeral ports problem.
 */
@ Log4j
public class AwesomeHttpServer {

	// Some auto-configuration
	private static final Integer countOfProcessors = Runtime.getRuntime ().availableProcessors ();
	private static final Integer countOfAcceptors = countOfProcessors / 4; // Assuming an 8-core machine, 2 acceptors should be enough
	private static final Integer countOfExchanges = countOfProcessors * 4; // Assuming an 8-core machine and waiting on network most of the time;
	private static final ConcurrentHashMap<Integer, HttpServer> knownServers = new ConcurrentHashMap<Integer, HttpServer> (); // Memoize

	// Privates
	private static final ExecutorService concurrentAcceptors = Executors.newFixedThreadPool (countOfAcceptors);
	private static final ExecutorService concurrentExchanges = Executors.newFixedThreadPool(countOfExchanges);

	/**
	 * For each of the available acceptors, tuned to the machine,
	 * try to open an HttpServer on an OS-supplied port (I've avoided to
	 * hard-code 8080, or 8000 because it may be used by something when
	 * you run the example)
	 */
	public void runIt () {
		// Submit the starting code and just wait
		concurrentAcceptors.execute (() -> {
			// !!
			try {
				// Go
				for (Integer objI = 0; objI < countOfAcceptors; ++objI) {
					// Make a new one
					final HttpServer oneServer = HttpServer.create (new InetSocketAddress ("0.0.0.0", objI == 0 ? 8080 : 0), 0);
					oneServer.createContext ("/", new HttpMethodsRouter ());
					oneServer.setExecutor (concurrentExchanges);
					knownServers.putIfAbsent (objI, oneServer);

					oneServer.getAddress ().getAddress ();
					// Print our ports
					oneLogger.info (String.format ("Listening on: (http://%s:%d)",
						InetAddress.getLocalHost ().getHostName (),
						oneServer.getAddress ().getPort ()));

					// Start it
					oneServer.start ();
				}
			} catch (final Exception e) {
				// Log this down
				oneLogger.error (e);
			} finally {
				// Clean it up
			}
		});

		// !!
		try {
			// Wait for it
			concurrentAcceptors.shutdown ();
			concurrentAcceptors.awaitTermination (Long.MAX_VALUE, TimeUnit.DAYS); // Right next to the next Big Bang;
		} catch (final InterruptedException e) {
			// Log this down
			oneLogger.error (e);
		} finally {
			// Clean-up
		}
	}
}
