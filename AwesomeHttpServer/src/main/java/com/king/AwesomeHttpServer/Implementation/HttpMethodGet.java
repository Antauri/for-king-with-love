package com.king.AwesomeHttpServer.Implementation;

// Imports
import java.io.IOException;
import java.io.OutputStream;import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * Dispatches based on GET method request path
 * to specific implementations for each of the "controllers"
 * that our application requires;
 */
public class HttpMethodGet implements HttpHandler {

	// Privates
	private static final Pattern knownAheadPatternForLogin = Pattern.compile ("/(\\d+)/login");
	private static final Pattern knownAheadPatternForScoreRetrieve = Pattern.compile ("/(\\d+)/highscorelist");

	/**
	 * This IS NOT how I'd make this for a project I am expecting to grow
	 * to reasonable size. Given that I can't use Spring or any other dedicated
	 * technology, I'd probably use reflection and map these to controllers/methods
	 * in a convention-over-configuration manner. But since this is just an interview
	 * I'm just going to hard-code the test requirements.
	 */
	@Override
	public void handle (final HttpExchange oneExchange) throws IOException {
		// !!
		try {
			// Get outputs
			final String onePath = oneExchange.getRequestURI ().getPath ();
			final String controllerLoginOut = controllerLogin (onePath);
			final String controllerHighscoreOut = controllerHighscore (onePath);
			String onePayload = new String ();

			// Self-check controllers
			if (controllerLoginOut.length () == 0 && controllerHighscoreOut.length () == 0) {
				// Default
				throw new IllegalStateException ("No controller found!");
			} else {
				// Check which one
				onePayload = controllerLoginOut.length () > 0 ?
					controllerLoginOut : controllerHighscoreOut;
			}

			// Write it out
			oneExchange.sendResponseHeaders (200, onePayload.length ());
			final OutputStream oneOs = oneExchange.getResponseBody ();
			oneOs.write (onePayload.getBytes ());
			oneOs.flush ();
		} catch (final Exception e) {
			// Default
			DefaultYoMamaJoke.yoMama (oneExchange);
		} finally {
			// Clean-up
			oneExchange.close ();
		}
	}

	// GET == /<lvid>/highscorelist
	public static String controllerHighscore (final String onePath) throws IOException {
		// Defaults
		String toReturn = new String ();
		final Matcher oneMatcherForScoreRetrieve = knownAheadPatternForScoreRetrieve.matcher (onePath);
		final Boolean isOnScoreRetrieveController = oneMatcherForScoreRetrieve.matches ();
		oneMatcherForScoreRetrieve.reset ();

		if (isOnScoreRetrieveController && oneMatcherForScoreRetrieve.find ()) {
			// Retrieve
			final Long oneLvId = Long.valueOf (oneMatcherForScoreRetrieve.group (1));

			// Check
			if (Ladder.getScoreLadder ().containsKey (oneLvId)) {
				// For each in this ladder, print-out CSV
				final ConcurrentNavigableMap<Long, Long> oneBucket = Ladder.getScoreLadder ().get (oneLvId).descendingMap ();
				final StringBuffer oneBufferToDumpTheCsvIn = new StringBuffer ();

				// For-each
				for (final Long oneKey : oneBucket.keySet ()) {
					// Prepare
					oneBufferToDumpTheCsvIn
						.append (oneBucket.get (oneKey))
						.append ("=")
						.append (String.valueOf (oneKey))
						.append (",");
				}

				// Retrieve the string
				String onePayload = oneBufferToDumpTheCsvIn.toString ();
				onePayload = onePayload.substring (0, onePayload.length () - 1); // Brute-force remove of last comma


				// We handled it
				toReturn = onePayload;
			} else {
				// Throw
				throw new IllegalStateException ("No such LVL id!");
			}
		}

		// Return
		return toReturn;
	}

	// GET == /<uid>/login
	public static String controllerLogin (final String onePath) throws IOException {
		// Defaults
		String toReturn = new String ();
		final Matcher oneMatcherForLogin = knownAheadPatternForLogin.matcher (onePath);
		final Boolean isOnLoginController = oneMatcherForLogin.matches ();
		oneMatcherForLogin.reset ();

		// Check
		if (isOnLoginController && oneMatcherForLogin.find ()) {
			// Get the UID
			final Long oneUserId = Long.valueOf (oneMatcherForLogin.group (1));
			UUID oneSessionId = null;

			/**
			 * Let's avoid three GET requests on different threads getting two
			 * different session keys, so we synchronize this piece of code
			 * so that between checking and add adding there is only one
			 * thread that passes through here;
			 */
			// Check
			oneSessionId = Session.getUserToSessionMapping ().containsKey (oneUserId) ?
				Session.getUserToSessionMapping ().get (oneUserId) :
					UUID.randomUUID ();

			// Put a few mappings
			Session.getUserToSessionMapping ().putIfAbsent (oneUserId, oneSessionId); // Session -> User
			Session.getSessionToUserMapping ().putIfAbsent (oneSessionId, oneUserId); // User -> Session
			Session.getDateToExpiringUserSession ().putIfAbsent (new Date ().getTime (), oneSessionId); // Date.now () - 10 minutes -> User -> Session

			// Respond
			final String oneSessionIdAsHex = oneSessionId.toString ();

			// Reset
			toReturn = oneSessionIdAsHex;
		}

		// Return
		return toReturn;
	}
}
