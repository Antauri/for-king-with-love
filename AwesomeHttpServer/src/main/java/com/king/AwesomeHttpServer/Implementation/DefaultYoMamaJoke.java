package com.king.AwesomeHttpServer.Implementation;

// Imports
import java.io.IOException;
import java.io.OutputStream;
import com.sun.net.httpserver.HttpExchange;

/**
 * Rude but funny reply to when nothing is
 * mapped for a specific route;
 */
public class DefaultYoMamaJoke {

	/**
	 * We used to had a joke in that we'd map any error or
	 * exception in our REST apis to a random yo-mama joke. The
	 * most popular was the server crashing joke;
	 * @param oneExchange
	 * @throws IOException
	 */
	public static void yoMama (final HttpExchange oneExchange) throws IOException {
		final String defaultAnswer = new String ("Yo mama so' fat this server crashed under her weight!");
		oneExchange.sendResponseHeaders (403, defaultAnswer.length ());
		final OutputStream oneOs = oneExchange.getResponseBody ();
		oneOs.write (defaultAnswer.getBytes ());
		oneOs.flush ();
	}
}
