package com.king.AwesomeHttpServer.Implementation;

// Imports
import lombok.Getter;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ConcurrentNavigableMap;

/**
 * Container for the LVL -> score -> user mappings
 * so that we can store the high-scores per LVL and only
 * keep the N ones that are the biggest in that bucket;
 */
public class Ladder {

    // Privates
    @ Getter private static final Long countOfItemsInBucket = 15L;
    @ Getter private static final ConcurrentSkipListMap<Long, ConcurrentSkipListMap<Long, Long>> scoreLadder =
        new ConcurrentSkipListMap<Long, ConcurrentSkipListMap<Long, Long>> ();

    /**
     * Method adds a score to the ladder IFF the promoted score
     * is higher than the N'th score in this bucket;
     */
    public static void addScore (final Long oneBucket, final Long oneScore, final Long oneUserId) {
        // Check
        if (!(scoreLadder.containsKey (oneBucket))) {
            // Make the bucket
            scoreLadder.put (oneBucket, new ConcurrentSkipListMap<Long, Long> ());
        }

        // Get
        final ConcurrentSkipListMap<Long, Long> scoreBucket = scoreLadder.get (oneBucket);

        // Check
        if (scoreBucket.size () > 0) {
        	// Get again
    		final ConcurrentNavigableMap<Long, Long> onlyNEntries = scoreBucket.subMap (scoreBucket.firstKey (), scoreBucket.lastKey ());

			// Check
			if (onlyNEntries.size () <= countOfItemsInBucket) {
			    // Add directly
				scoreBucket.put (oneScore, oneUserId);

				// Maintenance
				keepOnlyN (scoreBucket);
			} else {
				// Maintenance
				final ConcurrentNavigableMap<Long, Long> descendingMap = keepOnlyN (scoreBucket);

				// Now check least entry
				if (descendingMap.lastKey () < oneScore) {
					// Remove OLD, add new
					descendingMap.remove (descendingMap.firstKey ());
					descendingMap.put (oneScore, oneUserId);
				} else {
					// Ignore, score is not worth it
				}
			}
        } else {
        	// Add it as first entry here
        	scoreBucket.put (oneScore, oneUserId);
        }
    }

    // Keeps only N
    private static ConcurrentNavigableMap<Long, Long> keepOnlyN (final ConcurrentSkipListMap<Long, Long> scoreBucket) {
	    // Descend for only top N scores
		final ConcurrentNavigableMap<Long, Long> descendingMap = scoreBucket.descendingMap ();

		// Erase
		Long countOfNScoresOnly = 0L;

		// Go
		for (final Long oneKey : descendingMap.keySet ()) {
			// Check
			if (countOfNScoresOnly++ >= countOfItemsInBucket) {
				// Remove
				descendingMap.remove (oneKey);
			}
		}

		// Return
		return descendingMap;
    }
}
