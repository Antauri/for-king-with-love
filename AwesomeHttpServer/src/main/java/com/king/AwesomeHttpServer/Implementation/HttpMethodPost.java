package com.king.AwesomeHttpServer.Implementation;

import java.io.BufferedReader;
// Imports
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * Dispatches based on GET method request path
 * to specific implementations for each of the "controllers"
 * that our application requires;
 */
public class HttpMethodPost implements HttpHandler {

	// Privates
	private static final Pattern knownAheadPatternForScorePost = Pattern.compile ("/(\\d+)/score\\?sessionkey=(\\S+)");

	/**
	 * This IS NOT how I'd make this for a project I am expecting to grow
	 * to reasonable size. Given that I can't use Spring or any other dedicated
	 * technology, I'd probably use reflection and map these to controllers/methods
	 * in a convention-over-configuration manner. But since this is just an interview
	 * I'm just going to hard-code the test requirements.
	 */
	@Override
	public void handle (final HttpExchange oneExchange) throws IOException {
		// !!
		try {
			// Defaults
			final String onePath = oneExchange.getRequestURI ().getPath () + new String ("?") + oneExchange.getRequestURI ().getQuery ();
			final Boolean controllerScorePostOut = controllerScorePost (onePath, oneExchange.getRequestBody ());

			// Check
			if (controllerScorePostOut) {
				/**
				 * Initially we'd send 200 with a 0 content
				 * length header. Both statuses make sense but for
				 * the POST response body if the specification asked for
				 * no-content to be replied then it's better if we use
				 * the actual HTTP status code designed for the 'No Content'
				 * use case which is 204. All clients should work without
				 * issues, the only 'problem' is a complaining HttpServer on the logs
				 * that it will force content-length to -1;
				 */

				// Everything is OK
				oneExchange.sendResponseHeaders (204, 0); // 204 (No Content)
			} else {
				// Default
				throw new IllegalStateException ("No controller found!");
			}
		} catch (final Exception e) {
			// Default
			DefaultYoMamaJoke.yoMama (oneExchange);
		} finally {
			// Clean-up
			oneExchange.close ();
		}
	}

	// POST == /lvid/score?sessionkey=?
	public static Boolean controllerScorePost (final String onePath, final InputStream oneInputStream) throws IOException {
		// Defaults
		Boolean toReturn = false;
		final Matcher oneMatcher = knownAheadPatternForScorePost.matcher (onePath);
		final Boolean isOnScorePostController = oneMatcher.matches ();

		// Reset
		oneMatcher.reset ();

		// Check
		if (isOnScorePostController && oneMatcher.find ()) {
			// Process the session-key, check if it exists
			final Long oneLvId = Long.valueOf (oneMatcher.group (1));
			final UUID oneSessionKey = UUID.fromString (oneMatcher.group (2));

			// Check
			if (Session.getSessionToUserMapping ().containsKey (oneSessionKey)) {
				// Found someone, store high-score
				final Long oneUserId = Session.getSessionToUserMapping ().get (oneSessionKey);

				// Reading
				final InputStreamReader inputStreamReader =  new InputStreamReader (oneInputStream, "UTF-8");
				final BufferedReader bufferedReader = new BufferedReader (inputStreamReader);

				// Read
				int oneByte;
				final StringBuilder oneBuffer = new StringBuilder ();
				while ((oneByte = bufferedReader.read ()) != -1) {
				    oneBuffer.append ((char) oneByte);
				}

				// Close
				bufferedReader.close ();
				inputStreamReader.close ();

				// Add score
				final Long oneScore = Long.valueOf (oneBuffer.toString ());
				Ladder.addScore (oneLvId, oneScore, oneUserId);

				// Set
				toReturn = true;
			} else {
				// Default
				throw new IllegalStateException ("No session found!");
			}
		}

		// Return
		return toReturn;
	}
}
