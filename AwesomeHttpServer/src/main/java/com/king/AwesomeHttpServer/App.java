package com.king.AwesomeHttpServer;

import com.king.AwesomeHttpServer.Implementation.AwesomeHttpServer;

/**
 * Project consists of writing a pure Java-only
 * high-performance, concurrent HTTP server using as little
 * dependencies as possible (junit). I'll make an exception
 * here and add Lombok also, because it would be too tiring
 * to read all the getters/setters for each and every POJO
 * we would require.
 *
 * Also, I could not find com.sun.net.HttpServer in my default
 * Oracle JDK so I had to included in a portable manner, thus
 * a Maven project was a right choice (or Gradle);
 */
public class App {
	/**
	 * We'd be parsing CLI arguments with Apache Commons CLI
	 * or Spring here, but since this is just an interview, we'll
	 * just delegate to an implementation class and leave it there;
	 * @param args
	 */
    public static void main (final String[] args) {
    	// Get an instance
    	final AwesomeHttpServer theImplementation = new AwesomeHttpServer ();
    	theImplementation.runIt ();
    }
}
