package com.king.AwesomeHttpServer;

// Imports
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import com.king.AwesomeHttpServer.Implementation.HttpMethodGet;
import com.king.AwesomeHttpServer.Implementation.HttpMethodPost;
import com.king.AwesomeHttpServer.Implementation.Ladder;

import lombok.extern.log4j.Log4j;

/**
 * Unit test for simple App.
 */
@ Log4j
public class AppTest  extends TestCase {

	// Privates
	private static final ExecutorService concurrentTesters = Executors.newFixedThreadPool (Runtime.getRuntime ().availableProcessors ());

    // C'tor
    public AppTest (final String testName) {
        super (testName);
    }

    // Suite
    public static Test suite() {
        return new TestSuite (AppTest.class);
    }

    // Go
    public void testApp () throws IOException, InterruptedException, ExecutionException {
    	// Remember
    	final ArrayList<Future<Boolean>> futuresList = new ArrayList<Future<Boolean>> ();
    	final Boolean withLogging = false;
    	final Integer maxNumberOfLevels = 30;
    	final Integer timesToRun = 1000000;

    	// For
    	for (Integer objI = 0; objI < timesToRun; ++objI) {
        	// Submit
        	futuresList.add (concurrentTesters.submit (() -> {
    	    	// Defaults
    	    	final Random randomGenerator = new Random ();


        		// Create a new path
        		final Integer currentUserId = randomGenerator.nextInt (Integer.MAX_VALUE);
        		final String onePath = String.format ("/%d/login", currentUserId);
        		final String oneSessionKey = HttpMethodGet.controllerLogin (onePath);

        		// With the session ID and an random score, push it to the ladder
        		final Integer currentLvId = randomGenerator.nextInt (maxNumberOfLevels);
        		final Integer currentScore = randomGenerator.nextInt (Integer.MAX_VALUE);
        		final String onePostPath = String.format ("/%d/score?sessionkey=%s", currentLvId, oneSessionKey);
        		HttpMethodPost.controllerScorePost (onePostPath, new ByteArrayInputStream (Integer.toString (currentScore).getBytes ()));

        		// Check
        		if (withLogging) {
	        		// Log this down
	        		oneLogger.error (String
	        			.format ("SID (%s) for UID (%d) has posted score (%d) on LVL (%d)",
	    					oneSessionKey, currentUserId, currentScore, currentLvId));
        		}

    	    	// Return
    	    	return true;
        	}));
    	}

    	// Wait for'em
    	for (final Future<Boolean> oneFuture : futuresList) {
    		// Get
    		oneFuture.get ();
    	}

    	// Check the LVLs
    	for (Integer objI = 0; objI < maxNumberOfLevels; ++objI) {
    		// Defaults
    		final String onePath = String.format ("/%d/highscorelist", objI);
    		final String highscoreOutput = HttpMethodGet.controllerHighscore (onePath);

    		// Check they have at most N;
    		assert (highscoreOutput.split (",").length == Ladder.getCountOfItemsInBucket ());

    		// Log this down
    		oneLogger.error (String
				.format ("LVL (%d) has following highscore (%s)",
					objI, highscoreOutput));
    	}
    }
}
